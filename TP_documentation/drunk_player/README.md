
# Drunk_player
## Description

Drunk_player est un système de lecture des vidéos qui a trop bu.

Drunk_player est composé :

- une bibliothèque
- un programme graphique 

## Dépendances

- opencv 
- Boost

### Compilation 

```
mkdir build
cd build
cmake ..
make
```


![description](www.google.com)
